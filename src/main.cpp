/*******************************************************************************************
*
*   raylib [core] example - Basic window
*
*   Welcome to raylib!
*
*   To test examples, just press F6 and execute raylib_compile_execute script
*   Note that compiled executable is placed in the same folder as .c file
*
*   You can find all basic examples on C:\raylib\raylib\examples folder or
*   raylib official webpage: www.raylib.com
*
*   Enjoy using raylib. :)
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2013-2016 Ramon Santamaria (@raysan5)
*
********************************************************************************************/

#include <string.h>
// FIXME this namespace is a little excessive atm
namespace raylib {
    #include "raylib.h"
}

void rlib_camera_init(raylib::Camera *rlib_camera)
{
    rlib_camera->position = (raylib::Vector3) { 18.83f, 12.89f, 12.0f };  // approximations for trimetric (i think)
    rlib_camera->target = (raylib::Vector3) { 0.0f, 0.0f, 0.0f };      // Camera looking at point
    rlib_camera->up = (raylib::Vector3) { 0.0f, 1.0f, 0.0f };          // Camera up vector (rotation towards target)
    rlib_camera->fovy = 45.0f;                                // Camera field-of-view Y
    rlib_camera->projection = raylib::CAMERA_PERSPECTIVE;                   // Camera mode type

    SetCameraMode(*rlib_camera, raylib::CAMERA_CUSTOM);
}

void process_input(raylib::Vector3 *cube_position, raylib::Camera *rlib_camera)
{
    if (raylib::IsKeyPressed(raylib::KEY_W)) {
        cube_position->x -= 1.0;
        rlib_camera->position.x -= 1.0;
        rlib_camera->target.x -= 1.0;
    } else if (raylib::IsKeyPressed(raylib::KEY_S)) {
        cube_position->x += 1.0;
        rlib_camera->position.x += 1.0;
        rlib_camera->target.x += 1.0;
    }
    if (raylib::IsKeyPressed(raylib::KEY_D)) {
        cube_position->z -= 1.0;
        rlib_camera->position.z -= 1.0;
        rlib_camera->target.z -= 1.0;
    } else if (raylib::IsKeyPressed(raylib::KEY_A)) {
        cube_position->z += 1.0;
        rlib_camera->position.z += 1.0;
        rlib_camera->target.z += 1.0;
    }
}

int main(void)
{
    // Initialization
    //--------------------------------------------------------------------------------------
    const int screenWidth = 800;
    const int screenHeight = 450;

    raylib::InitWindow(screenWidth, screenHeight, "mmorp/g/ WIP demo");
    // TODO add compile option for log levels
    raylib::SetTraceLogLevel(raylib::LOG_DEBUG);

    raylib::SetTargetFPS(60);               // Set our game to run at 60 frames-per-second

    raylib::Camera rlib_camera = { 0 };
    rlib_camera_init(&rlib_camera);

    raylib::Vector3 cube_position = { 0.0f, 1.0f, 0.0f };
    raylib::Vector3 cube_size = { 2.0f, 2.0f, 2.0f };

    raylib::Vector3 prev_cube_pos = { 0 };
    memcpy(&prev_cube_pos, &cube_position, sizeof(raylib::Vector3));
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (!raylib::WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        process_input(&cube_position, &rlib_camera);
        raylib::UpdateCamera(&rlib_camera);
        //----------------------------------------------------------------------------------

        // Draw
        //----------------------------------------------------------------------------------
        raylib::BeginDrawing();

            raylib::ClearBackground(raylib::RAYWHITE);

            raylib::BeginMode3D(rlib_camera);

                raylib::DrawCube(cube_position, cube_size.x, cube_size.y, cube_size.z, raylib::RED);

                raylib::DrawGrid(50, 1.0f);

            raylib::EndMode3D();

            //DrawText("Congrats! You created your first window!", 190, 200, 20, LIGHTGRAY);

        raylib::EndDrawing();

        // TODO: this is for demo, remove this
        if (memcmp(&prev_cube_pos, &cube_position, sizeof(raylib::Vector3)) != 0) {
            raylib::TraceLog(raylib::LOG_DEBUG, "CUBE: %.1f, %.1f, %.1f", cube_position.x, cube_position.y, cube_position.z);
            memcpy(&prev_cube_pos, &cube_position, sizeof(raylib::Vector3));
        }
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    raylib::CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}
